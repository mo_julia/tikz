module TikZ

function plot(x,y;O=:auto,xtick=:auto,ytick=:auto,Grid=true,
        Address="/tmp/MoTikZ/",Plot_name="MoPlot", Size=(8,5),color=:auto,ylims=(-Inf,Inf))
    if color == :auto
        cols = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"]
    else
        cols = color
    end
    rgbcols = [parse.(Int, [cols[i][2:3],cols[i][4:5],cols[i][6:7]], base = 16) for i in 1:length(cols)]
    
    xlen=Size[1]
    ylen=Size[2]
    if typeof(x) == Array{Float64,2}
        n = size(x)[2]
    elseif typeof(x) == Array{Float64,1}
        n = 1
    else
        error("x and y must be an Array of dim 1 or 2")
    end
    color=[rgbcols[i] for i in 1:n]

    xl = size(x)[1]
    if !ispath(Address*Plot_name)
        mkpath(Address*Plot_name)
    end
    for j in 1:n
        FileName = Address*Plot_name*"/"*Plot_name*string(j)*".data"
        open(FileName, "w") do io
            for i in 1:xl
                if ylims[1] < y[i,j] < ylims[2]
                    write(io, string(x[i,j])*" "*string(y[i,j])*"\n")
                end
            end
        end;
    end
        
    xmax0 = maximum(x)
    xmin0 = minimum(x)
    
    ymax0 = min(maximum(y),ylims[2]);
    ymin0 = max(minimum(y),ylims[1]);
    

        
    
    if O == :auto
        ox = xmin0
        oy = ymin0
    else
        ox = O[1]
        oy = O[2]
    end
    
    xrange0 = xmax0-xmin0
    yrange0 = ymax0-ymin0
    
    if xtick == :auto
        ux0 = round(xrange0/6,sigdigits=3)
        xtick=round.((xmin0):ux0:(xmax0),sigdigits=3)
    end
    
    if ytick == :auto
        uy0 = round(yrange0/6,sigdigits=3)
        ytick=round.((ymin0):uy0:(ymax0),sigdigits=3)
    end
#     if Grid == :auto
#         xgrid_step=ux0
#         ygrid_step=uy0
#     end
#     if (Grid != false) & (Grid != :auto)
#         xgrid_step=Grid[1]
#         ygrid_step=Grid[2]
#     end
    xmin = min(ox ,xmin0) - 0.02*xrange0
    xmax = xmax0 + 0.02*xrange0
    
    ymin = min(oy , ymin0) - 0.02*yrange0
    ymax = ymax0 + 0.02*yrange0
    
    xrange = xmax-xmin
    yrange = ymax-ymin
    
    xu = xlen/xrange
    yu = ylen/yrange
    
    sxtick = string(collect(xtick))
    sxtick = replace(sxtick,"["=>"{")
    sxtick = replace(sxtick,"]"=>"}")
    
    sytick = string(collect(ytick))
    sytick = replace(sytick,"["=>"{")
    sytick = replace(sytick,"]"=>"}")
    FileNameTex = Address*Plot_name*"/"*Plot_name*".tex"
    open(FileNameTex, "w") do io
        write(io,"\\begin{tikzpicture}[x="*string(xu)*"cm,y="*string(yu)*"cm]\n")
#         if Grid != false
#          write(io,"\\draw[xstep="*string(xgrid_step)*",ystep="*string(ygrid_step)*
#                  ",densely dashed,color=lightgray,very thin] ("*
#                  string(xtick[1])*","*string(ytick[1])*
#                 ") grid ("*string(xtick[end])*","*string(ytick[end])*");\n")
#         end
        if Grid == true
            for i in 1:(length(xtick))
                write(io,"\\draw[line width=0.05mm,color=lightgray,dashed] ("*string(xtick[i])*","*string(ymin)*
                 ") -- ("*string(xtick[i])*","*string(ymax)*");\n")
            end
            for i in 1:(length(ytick))
                write(io,"\\draw[line width=0.05mm,color=lightgray,dashed] ("*string(xmin)*","*string(ytick[i])*
                 ") -- ("*string(xmax)*","*string(ytick[i])*");\n")
            end
        end
        ###### x axis:
        write(io,"\\draw[line width=0.15mm ,->] ("*string(xmin)*","*string(oy)*
                 ") -- ("*string(xmax)*","*string(oy)*") node[right] {\$x\$};%x axis\n")
        ##### y axis
        write(io, "\\draw[line width=0.15mm ,->] ("*string(ox)*","*string(ymin)*
                  ") -- ("*string(ox)*","*string(ymax)*") node[above] {\$y\$};%y axis\n")
        write(io,"\\foreach \\x  in "*sxtick*"\n")
        write(io,"   \\draw[shift={(\\x,"*string(oy)*")}] (0pt,0pt) -- (0pt,-2pt) node[below] {{\\tiny \\x}};\n")
        write(io,"\\foreach \\y  in "*sytick*"\n")
        write(io,"   \\draw[shift={("*string(ox)*",\\y)}] (0pt,0pt) -- (-2pt,0pt) node[left] {{\\tiny \\y}};\n")
        for j in 1:n
            write(io, "\\draw[color="*"{rgb,256:red,"*string(color[j][1])*"; green,"*
    string(color[j][2])*"; blue,"*string(color[j][3])*"}"*",line width=0.2mm] plot file {"*Plot_name*string(j)*".data};%"*Plot_name*"\n")
        end
        write(io,"\\end{tikzpicture}")
    end;
    FileAddresstemp = Address*Plot_name*"/temp.tex"
    open(FileAddresstemp, "w") do io
        write(io,"\\documentclass{standalone}\n")
        write(io,"\\usepackage{tikz}\n")
        write(io,"\\begin{document}\n")
        write(io,"\\input{"*FileNameTex*"}\n")
        write(io,"\\end{document}")
    end
    Path = pwd()
    cd(Address*Plot_name)
 
    open("/tmp/BASH_SCRIPT_for_My_TikZ.sh", "w") do io
        write(io,"pdflatex "*FileAddresstemp*" > /dev/null 2>&1")
    end
    run(`chmod +x /tmp/BASH_SCRIPT_for_My_TikZ.sh`)
    run(`/tmp/BASH_SCRIPT_for_My_TikZ.sh`)
    FileAddresspdf = Address*Plot_name*"/temp.pdf"
    run(`xdg-open $FileAddresspdf`)
    cd(Path)
    rm(Address*Plot_name*"/temp.aux")
    rm(Address*Plot_name*"/temp.log")
end

end # module
